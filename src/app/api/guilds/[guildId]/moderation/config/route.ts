import { NextRequest, NextResponse } from 'next/server';
import { getToken } from 'next-auth/jwt';

// eslint-disable-next-line import/prefer-default-export
export async function POST(
	req: NextRequest,
	{ params }: { params: { guildId: string } },
) {
	const token = await getToken({ req });

	if (!token) {
		return new NextResponse('Not signed in', {
			status: 401,
		});
	}
	const body = await req.json();

	await fetch(`${process.env.BACKEND_URL}/guilds/${params.guildId}/moderation/config`, {
		method: 'PUT',
		body: JSON.stringify(body),
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
			'Content-Type': 'application/json',
		},
	});

	return new NextResponse(null, {
		status: 204,
	});
}
