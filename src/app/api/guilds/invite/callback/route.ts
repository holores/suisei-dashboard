import { NextResponse } from 'next/server';
import { prisma } from '@/lib/prisma';

// eslint-disable-next-line import/prefer-default-export
export async function GET(req: Request) {
	const { searchParams } = new URL(req.url);
	const error = searchParams.get('error');
	const guildId = searchParams.get('guild_id');
	const code = searchParams.get('code');
	const stateId = searchParams.get('state');

	if (error) {
		if (stateId) {
			try {
				await prisma.inviteState.delete({
					where: {
						stateId,
					},
				});
			} catch { /* */ }
		}

		return new NextResponse(
			'Either the request has been cancelled or something went wrong. You may close this tab and try again if needed.',
			{ status: 400 },
		);
	}

	if (!guildId || !code || !stateId) {
		return new NextResponse('Invalid request', { status: 400 });
	}

	let state;
	try {
		state = await prisma.inviteState.delete({
			where: {
				stateId,
			},
		});
	} catch {
		return new NextResponse('State not found', { status: 400 });
	}

	if (!state) {
		return new NextResponse('State not found', { status: 400 });
	}

	if (state.guildId !== guildId) {
		return new NextResponse('Invalid state', { status: 400 });
	}

	try {
		const res = await fetch('https://discord.com/api/v10/oauth2/token', {
			body: new URLSearchParams([
				['client_id', process.env.DISCORD_CLIENT_ID!],
				['client_secret', process.env.DISCORD_CLIENT_SECRET!],
				['grant_type', 'authorization_code'],
				['code', code],
				['redirect_uri', `${process.env.NEXTAUTH_URL}/api/guilds/invite/callback`],
			]),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			method: 'POST',
		});

		if (res.status >= 400) {
			return new NextResponse('Something went wrong, try again!', { status: 500 });
		}
		return new NextResponse('Bot has been added successfully! You may close this tab now.');
	} catch (e) {
		return new NextResponse('Something went wrong, try again!', { status: 500 });
	}
}
