import { NextRequest, NextResponse } from 'next/server';
import { prisma } from '@/lib/prisma';
import { REST } from '@discordjs/rest';
import { RESTGetAPICurrentUserGuildsResult, Routes } from 'discord-api-types/v10';
import { getToken } from 'next-auth/jwt';

// eslint-disable-next-line import/prefer-default-export
export async function GET(req: NextRequest) {
	const { searchParams } = new URL(req.url);
	const guildId = searchParams.get('guild_id');

	if (!guildId) return new NextResponse('Invalid request', { status: 400 });

	const token = await getToken({ req });

	if (!token) {
		return new NextResponse('Not signed in', {
			status: 401,
		});
	}

	if (!token.access_token) return new NextResponse('No account found', { status: 400 });

	const rest = new REST({
		authPrefix: 'Bearer',
		version: '10',
	}).setToken(token.access_token);

	const guilds = await rest.get(Routes.userGuilds()) as RESTGetAPICurrentUserGuildsResult;

	const filteredGuilds = guilds
		// eslint-disable-next-line no-bitwise
		.filter((guild) => guild.owner || Number.parseInt(guild.permissions, 10) & 0x20)
		.map((guild) => guild.id);

	if (!filteredGuilds.includes(guildId)) return new NextResponse('Invalid request', { status: 400 });

	const state = await prisma.inviteState.create({
		data: {
			guildId,
		},
	});

	return NextResponse.redirect(`https://discord.com/oauth2/authorize?client_id=${process.env.DISCORD_CLIENT_ID}&permissions=8&redirect_uri=${encodeURIComponent(`${process.env.NEXTAUTH_URL}/api/guilds/invite/callback`)}&response_type=code&scope=identify%20guilds%20bot%20applications.commands&guild_id=${guildId}&state=${state.stateId}&disable_guild_select=true`, 303);
}
