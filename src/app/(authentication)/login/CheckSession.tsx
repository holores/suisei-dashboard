'use client';

import { getSession } from 'next-auth/react';
import { useEffect } from 'react';
import { useRouter } from 'next/navigation';

export default function CheckSession() {
	const router = useRouter();

	useEffect(() => {
		if (!router) return;

		(async () => {
			const session = await getSession();
			if (session) router.push('/');
		})();
	}, [router]);

	return null;
}
