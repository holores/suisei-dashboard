import Link from 'next/link';
import SignOutButton from '../../components/ui/SignOutButton';

interface IProps {
	children: React.ReactNode;
}

export default function Layout({ children }: IProps) {
	return (
		<>
			<div className="fixed flex h-20 w-full items-center justify-between border-b-2 border-black/20 px-8 dark:border-white/30">
				<Link className="text-xl font-bold" href="/">
					Suisei&apos;s Mic
				</Link>
				<SignOutButton />
			</div>
			{children}
		</>
	);
}
