export default function Loading() {
	return (
		<div className="grid h-full min-h-screen w-full place-items-center">
			<p>Loading...</p>
		</div>
	);
}
