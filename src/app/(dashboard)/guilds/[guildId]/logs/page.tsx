import RetracedEventsBrowser from '@/components/ui/RetracedEventsBrowser';

async function getAuditLogToken(guildId: string) {
	const res = await fetch(`${process.env.BACKEND_URL}/guilds/${guildId}/auditLogToken`, {
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
		},
		cache: 'no-cache',
	});

	return res.text();
}

interface IProps {
	params: {
		guildId: string;
	}
}

export default async function LogsPage({ params: { guildId } }: IProps) {
	const auditLogToken = await getAuditLogToken(guildId);

	return (
		<div className="grid w-full place-items-center">
			<RetracedEventsBrowser
				auditLogToken={auditLogToken}
				host="https://retraced.suisei.app/auditlog/viewer/v1"
				header="Audit logs"
			/>
		</div>
	);
}
