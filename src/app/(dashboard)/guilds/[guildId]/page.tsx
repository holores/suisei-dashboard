export default function GuildPage() {
	return (
		<div className="grid w-full place-items-center">
			<p>Nothing to see here yet, check the sidebar.</p>
		</div>
	);
}
