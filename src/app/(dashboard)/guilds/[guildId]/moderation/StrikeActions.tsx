'use client';

import {
	Active, defaultDropAnimationSideEffects, DndContext, DragOverlay,
} from '@dnd-kit/core';
import {
	arrayMove, SortableContext, useSortable, verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { useState } from 'react';
import { CSS } from '@dnd-kit/utilities';
import { Button } from '@/components/ui/button';
import {
	Check, ChevronsUpDown, LucidePencil, LucidePlus, LucideTrash,
} from 'lucide-react';
import { StrikeConfigItem } from '@/types/ApiResponses';
import { nanoid } from 'nanoid';
import {
	Dialog, DialogContent, DialogFooter, DialogHeader, DialogTitle,
} from '@/components/ui/dialog';
import { Popover, PopoverContent } from '@/components/ui/popover';
import { PopoverTrigger } from '@radix-ui/react-popover';
import {
	Command, CommandEmpty, CommandGroup, CommandInput, CommandItem,
} from '@/components/ui/command';
import { cn } from '@/lib/utils';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { useRouter } from 'next/navigation';
import { Checkbox } from '@/components/ui/checkbox';

const moderationActions = [
	{
		label: 'Warn',
		value: 'WARN',
	},
	{
		label: 'Mute',
		value: 'MUTE',
	},
	{
		label: 'Kick',
		value: 'KICK',
	},
	{
		label: 'Ban',
		value: 'BAN',
	},
];

type IEditDialogInfo = {
	action: 'new';
	item: Partial<StrikeConfigItem>;
	internalId: string;
} | {
	action: 'edit';
	internalId: string;
	item: StrikeConfigItem;
};

interface ActionItemProps {
	item: StrikeConfigItem;
	id: string;
	setEditDialogInfo: (data: IEditDialogInfo) => void;
	deleteAction: (id: string) => void;
}

function ActionItem({
	item, id, setEditDialogInfo, deleteAction,
}: ActionItemProps) {
	const {
		attributes,
		isDragging,
		listeners,
		setActivatorNodeRef,
		setNodeRef,
		transform,
		transition,
	} = useSortable({ id });

	const style = {
		opacity: isDragging ? 0.4 : undefined,
		transform: CSS.Transform.toString(transform),
		transition,
	};

	return (
		<div
			ref={setNodeRef}
			style={style}
			className="my-4 flex w-full items-center justify-between rounded-lg border border-black/70 p-4 dark:border-white"
		>
			{moderationActions.find((action) => action.value === item.action)?.label}
			<div className="flex items-center gap-2">
				<Button
					className="mr-4 flex items-center gap-2"
					variant="destructive"
					onClick={() => deleteAction(id)}
				>
					<LucideTrash className="h-4 w-4" />
				</Button>
				<Button
					className="flex items-center gap-2"
					onClick={() => setEditDialogInfo({
						action: 'edit',
						item,
						internalId: id,
					})}
				>
					<LucidePencil className="h-4 w-4" />
				</Button>
				<button
					type="button"
					// eslint-disable-next-line tailwindcss/no-custom-classname
					className="DragHandle"
					// eslint-disable-next-line react/jsx-props-no-spreading
					{...attributes}
					// eslint-disable-next-line react/jsx-props-no-spreading
					{...listeners}
					ref={setActivatorNodeRef}
				>
					<svg viewBox="0 0 20 20" width="12">
						<path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z" />
					</svg>
				</button>
			</div>
		</div>
	);
}

interface IProps {
	strikeConfig: Array<StrikeConfigItem & { internalId: string }>;
	guildId: string;
}

export default function StrikeActions({ strikeConfig, guildId }: IProps) {
	const router = useRouter();

	const [actions, setActions] = useState(strikeConfig);

	const [dragActive, setActive] = useState<Active | null>(null);

	const [editDialogInfo, setEditDialogInfo] = useState<IEditDialogInfo | null>(null);
	const [popoverOpen, setPopoverOpen] = useState(false);
	const [durationEnabled, setDurationEnabled] = useState(false);

	async function saveConfig() {
		await fetch(`/api/guilds/${guildId}/moderation/strikeConfig`, {
			method: 'POST',
			body: JSON.stringify({
				actions,
			}),
			headers: {
				'Content-Type': 'application/json',
			},
			credentials: 'same-origin',
		});

		router.refresh();
	}

	function deleteAction(id: string) {
		const updatedActions = [...actions];
		const index = updatedActions.findIndex((item) => item.internalId === id);
		updatedActions.splice(index, 1);
		setActions(updatedActions);
	}

	return (
		<div className="flex h-full w-96 flex-col">
			<div className="h-full grow">
				<h1 className="mb-4 text-2xl font-semibold">Strikes setup</h1>
				<h4 className="mb-4 text-lg">(Drag and drop to change the order)</h4>
				<div className="max-h-96 overflow-y-auto">
					<DndContext
						onDragStart={({ active }) => {
							setActive(active);
						}}
						onDragEnd={({ active, over }) => {
							if (over && active.id !== over.id) {
								const activeIndex = actions.findIndex((item) => item.internalId === active.id);
								const overIndex = actions.findIndex((item) => item.internalId === over.id);
								setActions(arrayMove(actions, activeIndex, overIndex));
								setActive(null);
							}
						}}
						onDragCancel={() => {
							setActive(null);
						}}
					>
						<SortableContext
							items={actions.map((action) => action.internalId)}
							strategy={verticalListSortingStrategy}
						>
							{actions.map((item) => (
								<ActionItem
									item={item}
									setEditDialogInfo={setEditDialogInfo}
									deleteAction={(id) => deleteAction(id)}
									id={item.internalId}
									key={item.internalId}
								/>
							))}
						</SortableContext>
						<DragOverlay dropAnimation={{
							sideEffects: defaultDropAnimationSideEffects({
								styles: {
									active: {
										opacity: '0.4',
									},
								},
							}),
						}}
						>
							{dragActive
								? (
									<ActionItem
										item={actions.find((action) => action.internalId === dragActive.id)!}
										setEditDialogInfo={() => {}}
										deleteAction={() => {}}
										id={dragActive.id as string}
									/>
								)
								: null}
						</DragOverlay>
					</DndContext>
				</div>
			</div>
			<div className="mt-4 flex justify-end gap-4">
				<Button
					type="button"
					className="flex items-center gap-2"
					onClick={() => {
						setEditDialogInfo({
							action: 'new',
							item: {},
							internalId: nanoid(8),
						});
					}}
				>
					Add new
					<LucidePlus className="h-4 w-4" />
				</Button>
				<Button
					type="button"
					onClick={() => saveConfig()}
				>
					Save
				</Button>
			</div>

			<Dialog
				open={editDialogInfo !== null}
				onOpenChange={(open) => {
					if (!open) {
						setPopoverOpen(false);
						setDurationEnabled(false);
						setEditDialogInfo(null);
					}
				}}
			>
				<DialogContent>
					<DialogHeader>
						<DialogTitle>
							{
								editDialogInfo?.action === 'new'
									? 'Add action'
									: 'Edit action'
							}
						</DialogTitle>
					</DialogHeader>
					<div className="space-y-4">
						<Popover open={popoverOpen} onOpenChange={setPopoverOpen}>
							<PopoverTrigger asChild>
								<Button
									variant="outline"
									role="combobox"
									aria-expanded={popoverOpen}
									className="w-[200px] justify-between"
								>
									{editDialogInfo?.item.action
										? moderationActions
											.find((action) => action.value === editDialogInfo?.item.action)?.label
										: 'Select action...'}
									<ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
								</Button>
							</PopoverTrigger>
							<PopoverContent className="w-[200px] p-0">
								<Command>
									<CommandInput placeholder="Search action..." />
									<CommandEmpty>No action found.</CommandEmpty>
									<CommandGroup>
										{moderationActions.map((action) => (
											<CommandItem
												key={action.value}
												value={action.value}
												onSelect={() => {
													setEditDialogInfo({
														...editDialogInfo,
														item: {
															...editDialogInfo?.item,
															action: action.value,
														},
													} as any);
													setPopoverOpen(false);
												}}
											>
												<Check
													className={cn(
														'mr-2 h-4 w-4',
														editDialogInfo?.item.action === action.value ? 'opacity-100' : 'opacity-0',
													)}
												/>
												{action.label}
											</CommandItem>
										))}
									</CommandGroup>
								</Command>
							</PopoverContent>
						</Popover>
						{
							editDialogInfo?.item.action === 'BAN'
							&& (
								<div className="flex items-center space-x-2">
									<Checkbox
										id="durationEnabled"
										checked={durationEnabled}
										onClick={() => setDurationEnabled(!durationEnabled)}
										className="text-sm font-medium leading-none"
									/>
									<Label
										htmlFor="durationEnabled"
									>
										Enable duration
									</Label>
								</div>
							)
						}
						{
							(editDialogInfo?.item.action === 'MUTE'
								|| (editDialogInfo?.item.action === 'BAN' && durationEnabled))
							&& (
								<div>
									<Label
										htmlFor="actionDuration"
									>
										Duration (in seconds)
									</Label>
									<Input
										id="actionDuration"
										type="number"
										value={editDialogInfo.item.duration ?? undefined}
										min={0}
										onChange={(e) => {
											setEditDialogInfo({
												...editDialogInfo,
												item: {
													...editDialogInfo?.item,
													duration: e.target.valueAsNumber,
												},
											} as any);
										}}
									/>
								</div>
							)
						}
					</div>
					<DialogFooter>
						<Button
							type="button"
							onClick={() => {
								if (!editDialogInfo) {
									return;
								}

								if (
									editDialogInfo.item.action === 'MUTE'
									&& (!editDialogInfo.item.duration || editDialogInfo.item.duration <= 0)
								) {
									// TODO: Make this proper error on the form
									alert('Duration must be valid');
									return;
								}

								if (editDialogInfo.action === 'new') {
									setActions([
										...actions,
										{
											...editDialogInfo.item as any,
											duration: (
												editDialogInfo.item.action === 'MUTE' || durationEnabled
													? editDialogInfo.item.duration
													: undefined
											),
											internalId: editDialogInfo.internalId,
										},
									]);
								} else {
									const updatedActions = [...actions];
									const index = updatedActions
										.findIndex((item) => item.internalId === editDialogInfo.internalId);
									updatedActions[index] = {
										...editDialogInfo.item as any,
										duration: (
											editDialogInfo.item.action === 'MUTE' || durationEnabled
												? editDialogInfo.item.duration
												: undefined
										),
										internalId: editDialogInfo.internalId,
									};
									setActions(updatedActions);
								}

								setPopoverOpen(false);
								setDurationEnabled(false);
								setEditDialogInfo(null);
							}}
						>
							Save
						</Button>
					</DialogFooter>
				</DialogContent>
			</Dialog>
		</div>
	);
}
