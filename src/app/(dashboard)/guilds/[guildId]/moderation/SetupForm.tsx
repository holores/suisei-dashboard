'use client';

import { Button } from '@/components/ui/button';
import {
	Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage,
} from '@/components/ui/form';
import { useForm } from 'react-hook-form';
import { ChannelResponse, RoleResponse } from '@/types/ApiResponses';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import { Popover, PopoverContent, PopoverTrigger } from '@/components/ui/popover';
import { cn } from '@/lib/utils';
import { Check, ChevronsUpDown } from 'lucide-react';
import {
	Command, CommandEmpty, CommandGroup, CommandInput, CommandItem,
} from '@/components/ui/command';
import { Input } from '@/components/ui/input';
import { useRouter } from 'next/navigation';

const formSchema = z.object({
	muteRole: z.string(),
	logChannel: z.string(),
	strikeExpiresAfter: z.number(),
});

interface IProps {
	setupInfo: {
		channels: ChannelResponse;
		roles: RoleResponse;
	}
	guildId: string;
	config?: {
		muteRole: string;
		logChannel: string;
		strikeExpiresAfter: number;
	}
}

export default function SetupForm({ setupInfo, guildId, config }: IProps) {
	const router = useRouter();

	const form = useForm<z.infer<typeof formSchema>>({
		resolver: zodResolver(formSchema),
		defaultValues: {
			strikeExpiresAfter: config?.strikeExpiresAfter ?? 2592000,
			muteRole: config?.muteRole,
			logChannel: config?.logChannel,
		},
	});

	async function onSubmit(values: z.infer<typeof formSchema>) {
		await fetch(`/api/guilds/${guildId}/moderation/config`, {
			method: 'POST',
			body: JSON.stringify(values),
			headers: {
				'Content-Type': 'application/json',
			},
			credentials: 'same-origin',
		});

		router.refresh();
	}

	return (
		<div>
			<h1 className="mb-8 text-2xl font-semibold">Moderation setup</h1>
			{/* eslint-disable-next-line react/jsx-props-no-spreading */}
			<Form {...form}>
				<form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
					<FormField
						control={form.control}
						name="logChannel"
						render={({ field }) => (
							<FormItem className="flex flex-col">
								<FormLabel>Log channel</FormLabel>
								<Popover>
									<PopoverTrigger asChild>
										<FormControl>
											<Button
												variant="outline"
												role="combobox"
												className={cn(
													'w-[200px] justify-between',
													!field.value && 'text-muted-foreground',
												)}
											>
												{field.value
													? `#${setupInfo.channels.find(
														(channel) => channel.id === field.value,
													)!.name}`
													: 'Select channel'}
												<ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
											</Button>
										</FormControl>
									</PopoverTrigger>
									<PopoverContent className="w-[200px] p-0">
										<Command>
											<CommandInput placeholder="Search channel..." />
											<CommandEmpty>No channel found.</CommandEmpty>
											<CommandGroup>
												{setupInfo.channels.map((channel) => (
													<CommandItem
														value={channel.name}
														key={channel.id}
														onSelect={() => {
															form.setValue('logChannel', channel.id);
														}}
													>
														<Check
															className={cn(
																'mr-2 h-4 w-4',
																channel.id === field.value
																	? 'opacity-100'
																	: 'opacity-0',
															)}
														/>
														#
														{channel.name}
													</CommandItem>
												))}
											</CommandGroup>
										</Command>
									</PopoverContent>
								</Popover>
								<FormDescription>
									This is the channel that will be used for non-public moderation logs.
								</FormDescription>
								<FormMessage />
							</FormItem>
						)}
					/>

					<FormField
						control={form.control}
						name="muteRole"
						render={({ field }) => (
							<FormItem className="flex flex-col">
								<FormLabel>Mute role</FormLabel>
								<Popover>
									<PopoverTrigger asChild>
										<FormControl>
											<Button
												variant="outline"
												role="combobox"
												className={cn(
													'w-[200px] justify-between',
													!field.value && 'text-muted-foreground',
												)}
											>
												{field.value
													? `@${setupInfo.roles.find(
														(role) => role.id === field.value,
													)!.name}`
													: 'Select role'}
												<ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
											</Button>
										</FormControl>
									</PopoverTrigger>
									<PopoverContent className="w-[200px] p-0">
										<Command>
											<CommandInput placeholder="Search role..." />
											<CommandEmpty>No role found.</CommandEmpty>
											<CommandGroup>
												{setupInfo.roles.map((role) => (
													<CommandItem
														value={role.name}
														key={role.id}
														onSelect={() => {
															form.setValue('muteRole', role.id);
														}}
													>
														<Check
															className={cn(
																'mr-2 h-4 w-4',
																role.id === field.value
																	? 'opacity-100'
																	: 'opacity-0',
															)}
														/>
														@
														{role.name}
													</CommandItem>
												))}
											</CommandGroup>
										</Command>
									</PopoverContent>
								</Popover>
								<FormDescription>
									This is the role that will be used when muting users.
								</FormDescription>
								<FormMessage />
							</FormItem>
						)}
					/>

					<FormField
						control={form.control}
						name="strikeExpiresAfter"
						render={({ field }) => (
							<FormItem>
								<FormLabel>Strike duration</FormLabel>
								<FormControl>
									{/* eslint-disable-next-line react/jsx-props-no-spreading */}
									<Input type="number" min={0} {...field} />
								</FormControl>
								<FormDescription>
									This is the time in seconds before a strike will expire. Default is one month.
								</FormDescription>
								<FormMessage />
							</FormItem>
						)}
					/>

					<div className="flex justify-end">
						<Button type="submit">Submit</Button>
					</div>
				</form>
			</Form>
		</div>
	);
}
