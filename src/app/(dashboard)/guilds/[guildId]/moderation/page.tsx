import SetupForm from '@/app/(dashboard)/guilds/[guildId]/moderation/SetupForm';
import type { ChannelResponse, ModerationConfigResponse, RoleResponse } from '@/types/ApiResponses';
import { ChannelType } from 'discord-api-types/v10';
import StrikeActions from '@/app/(dashboard)/guilds/[guildId]/moderation/StrikeActions';
import { nanoid } from 'nanoid';

async function fetchModerationConfig(guildId: string) {
	const res = await fetch(`${process.env.BACKEND_URL}/guilds/${guildId}/moderation/config`, {
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
		},
		cache: 'no-cache',
	});

	const config = await res.json() as ModerationConfigResponse;
	if (!config) return null;

	return {
		...config,
		strikes: config.strikes.map((strike) => ({
			...strike,
			internalId: nanoid(8),
		})),
	};
}

async function fetchSetupInfo(guildId: string) {
	const rolesRes = await fetch(`${process.env.BACKEND_URL}/guilds/${guildId}/roles`, {
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
		},
		cache: 'no-cache',
	});

	const channelsRes = await fetch(`${process.env.BACKEND_URL}/guilds/${guildId}/channels`, {
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
		},
		cache: 'no-cache',
	});

	const roles = await rolesRes.json() as RoleResponse;
	const channels = await channelsRes.json() as ChannelResponse;
	const filteredChannels = channels.filter((channel) => channel.type === ChannelType.GuildText);

	return {
		roles,
		channels: filteredChannels,
	};
}

interface IProps {
	params: {
		guildId: string;
	}
}

export default async function ModerationPage({ params: { guildId } }: IProps) {
	const moderationConfig = await fetchModerationConfig(guildId);
	const setupInfo = await fetchSetupInfo(guildId);

	if (!moderationConfig) {
		return (
			<div className="grid w-full place-items-center">
				<SetupForm guildId={guildId} setupInfo={setupInfo} />
			</div>
		);
	}

	return (
		<div className="grid w-full place-items-center">
			<div className="flex gap-8">
				<div className="rounded-lg border border-black/70 p-6 dark:border-white">
					<SetupForm
						setupInfo={setupInfo}
						guildId={guildId}
						config={moderationConfig}
					/>
				</div>
				<div className="rounded-lg border border-black/70 p-6 dark:border-white">
					<StrikeActions
						guildId={guildId}
						strikeConfig={moderationConfig.strikes}
					/>
				</div>
			</div>
		</div>
	);
}
