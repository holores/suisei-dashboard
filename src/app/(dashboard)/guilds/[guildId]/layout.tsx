import GuildSidebar from '@/components/ui/GuildSidebar';
import { NextRequest } from 'next/server';
import { headers } from 'next/headers';
import { getToken } from 'next-auth/jwt';
import { redirect } from 'next/navigation';
import { REST } from '@discordjs/rest';
import { RESTGetAPICurrentUserGuildsResult, Routes } from 'discord-api-types/v10';

interface IProps {
	children: React.ReactNode;
	params: {
		guildId: string;
	}
}

async function fetchGuild(guildId: string) {
	const req = new NextRequest('https://dashboard.suisei.app/', {
		headers: headers(),
	});

	const token = await getToken({ req });
	if (!token) {
		redirect('/login');
	}

	if (!token?.access_token) throw new Error('Failed to find user');

	const rest = new REST({
		authPrefix: 'Bearer',
		version: '10',
	}).setToken(token.access_token);
	const guilds = await rest.get(Routes.userGuilds()) as RESTGetAPICurrentUserGuildsResult;

	return guilds.find((guild) => guild.id === guildId)!;
}

export default async function Layout({ children, params: { guildId } }: IProps) {
	const guild = await fetchGuild(guildId);

	return (
		<div className="flex min-h-screen min-w-full pt-20">
			<GuildSidebar guild={guild} />
			{children}
		</div>
	);
}
