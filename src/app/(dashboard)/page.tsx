import { Card } from '@/components/ui/card';
import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar';
import Link from 'next/link';
import { Button } from '@/components/ui/button';
import { RESTGetAPICurrentUserGuildsResult, Routes } from 'discord-api-types/v10';
import { getToken } from 'next-auth/jwt';
import { REST } from '@discordjs/rest';
import { headers } from 'next/headers';
import { NextRequest } from 'next/server';
import { redirect } from 'next/navigation';

async function fetchGuilds() {
	const req = new NextRequest('https://dashboard.suisei.app/', {
		headers: headers(),
	});

	const token = await getToken({ req });
	if (!token) {
		redirect('/login');
	}

	if (!token?.access_token) throw new Error('Failed to find user');

	const rest = new REST({
		authPrefix: 'Bearer',
		version: '10',
	}).setToken(token.access_token);

	const guilds = await rest.get(Routes.userGuilds()) as RESTGetAPICurrentUserGuildsResult;

	// eslint-disable-next-line no-bitwise,max-len
	const filteredGuilds = guilds.filter((guild) => guild.owner || Number.parseInt(guild.permissions, 10) & 0x20);

	const botGuildsRes = await fetch(`${process.env.BACKEND_URL}/guilds`, {
		headers: {
			Authorization: process.env.BACKEND_TOKEN as string,
		},
		cache: 'no-cache',
	});
	const botGuilds: string[] = await botGuildsRes.json();

	return filteredGuilds.map((guild) => ({
		id: guild.id,
		name: guild.name,
		icon: guild.icon,
		joined: botGuilds.includes(guild.id),
	}));
}

export default async function HomePage() {
	const guilds = await fetchGuilds();

	return (
		<div className="grid min-h-screen w-full place-items-center pt-24">
			<div className="grid max-w-2xl grid-cols-4 gap-4">
				{guilds.map((guild) => (
					<Card key={guild.id} className="flex flex-col items-center justify-between gap-4 px-4 py-6 text-center">
						<div className="flex flex-col items-center gap-2">
							<Avatar className="h-16 w-16">
								{
									guild.icon
									&& (
										<AvatarImage
											src={`https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.${guild.icon.startsWith('a_') ? 'gif' : 'webp'}?size=128`}
										/>
									)
								}
								<AvatarFallback>
									{guild.name.replace(/(\W_)+/g, '').split(' ').map((str) => str[0]).join('')}
								</AvatarFallback>
							</Avatar>
							<p>{guild.name}</p>
						</div>
						{
							guild.joined
								? (
									<Link href={`/guilds/${guild.id}`}>
										<Button type="button">
											Manage
										</Button>
									</Link>
								) : (
									<Link href={`/api/guilds/invite?guild_id=${guild.id}`} target="_blank">
										<Button type="button">
											Invite
										</Button>
									</Link>
								)
						}
					</Card>
				))}
			</div>
		</div>
	);
}
