import '@/styles/globals.css';
import { Inter } from 'next/font/google';
import DarkModeProvider from '@/components/providers/DarkModeProvider';
import ThemeSwitcher from '@/components/ui/ThemeSwitcher';
import SessionProvider from '@/components/providers/SessionProvider';

interface IProps {
	children: React.ReactNode;
}

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
	title: "Suisei's Mic - Dashboard",
	description: "The dashboard for Suisei's Mic.",
};

export default function RootLayout({ children }: IProps) {
	return (
		<html lang="en" suppressHydrationWarning>
			<head />
			<body className={inter.className}>
				<SessionProvider>
					<DarkModeProvider>
						{children}
						<ThemeSwitcher />
					</DarkModeProvider>
				</SessionProvider>
			</body>
		</html>
	);
}
