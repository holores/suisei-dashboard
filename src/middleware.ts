import { getToken } from 'next-auth/jwt';
import { withAuth } from 'next-auth/middleware';
import { NextResponse } from 'next/server';

export default withAuth(
	async (req) => {
		const token = await getToken({
			req,
		});
		const isAuthPage = req.nextUrl.pathname.startsWith('/login');

		if (isAuthPage) {
			if (token) {
				return NextResponse.redirect(new URL('/', req.url));
			}

			return null;
		}

		if (!token) {
			return NextResponse.redirect(new URL('/login', req.url));
		}

		return null;
	},
	{
		callbacks: {
			async authorized() {
				// This is a work-around for handling redirect on auth pages.
				// We return true here so that the middleware function above
				// is always called.
				return true;
			},
		},
	},
);

export const config = {
	matcher: [
		/*
		 * Match all request paths except for the ones starting with:
		 * - api (API routes)
		 * - _next/static (static files)
		 * - _next/image (image optimization files)
		 * - favicon.ico (favicon file)
		 */
		'/((?!api|_next/static|_next/image|favicon.ico).*)',
	],
};
