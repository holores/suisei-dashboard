'use client';

import OrigRetracedEventsBrowser, { RetracedEventsBrowserProps } from '@retracedhq/logs-viewer';
import { useTheme } from 'next-themes';

export default function RetracedEventsBrowser(props: RetracedEventsBrowserProps) {
	const { resolvedTheme } = useTheme();

	if (!resolvedTheme) {
		return null;
	}

	return (
		<OrigRetracedEventsBrowser
			// eslint-disable-next-line react/jsx-props-no-spreading
			{...props}
			admin={false}
			theme={resolvedTheme as 'light' | 'dark'}
		/>
	);
}
