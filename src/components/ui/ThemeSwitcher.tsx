'use client';

import { useEffect, useState } from 'react';
import { useTheme } from 'next-themes';
import { LucideMoon, LucideSun } from 'lucide-react';
import { Skeleton } from './skeleton';
import { Button } from './button';

export default function ThemeSwitcher() {
	const [mounted, setMounted] = useState(false);
	const { resolvedTheme, setTheme } = useTheme();

	// useEffect only runs on the client, so now we can safely show the UI
	useEffect(() => {
		setMounted(true);
	}, []);

	if (!mounted) {
		return (
			<Skeleton className="absolute bottom-8 right-8 h-12 w-12 rounded-full" />
		);
	}

	return (
		<Button
			variant="outline"
			className="absolute bottom-8 right-8 grid h-12 w-12 cursor-pointer place-items-center rounded-full border-2 border-accent"
			onClick={() => setTheme(resolvedTheme === 'light' ? 'dark' : 'light')}
		>
			{resolvedTheme === 'light' ? <LucideMoon className="h-6 w-6" /> : <LucideSun className="h-6 w-6" /> }
		</Button>
	);
}
