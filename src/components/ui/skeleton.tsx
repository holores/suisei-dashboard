import { cn } from '@/lib/utils';

function Skeleton({
	className,
	...props
}: React.HTMLAttributes<HTMLDivElement>) {
	return (
		<div
			className={cn('animate-pulse rounded-md bg-muted', className)}
			// eslint-disable-next-line react/jsx-props-no-spreading
			{...props}
		/>
	);
}

// eslint-disable-next-line import/prefer-default-export
export { Skeleton };
