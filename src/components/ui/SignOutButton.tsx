'use client';

import { signOut } from 'next-auth/react';
import { Button } from '@/components/ui/button';
import { LucideLogOut } from 'lucide-react';

export default function SignOutButton() {
	return (
		<Button
			type="button"
			variant="secondary"
			onClick={() => signOut()}
			className="flex items-center gap-2"
		>
			<LucideLogOut className="h-4 w-4" />
			Sign out
		</Button>
	);
}
