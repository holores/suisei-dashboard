import Link from 'next/link';
import { LucideHammer, LucideHome, LucideScrollText } from 'lucide-react';
import type { RESTAPIPartialCurrentUserGuild } from 'discord-api-types/v10';
import { Button } from './button';
import { Avatar, AvatarFallback, AvatarImage } from './avatar';

export default function GuildSidebar({ guild }: { guild: RESTAPIPartialCurrentUserGuild }) {
	return (
		<div className="min-h-full w-1/12 min-w-[300px] border-r border-black/20 px-4 py-8 dark:border-white/30">
			<h1 className="flex items-center gap-2 text-center text-xl font-medium">
				<Avatar className="h-12 w-12">
					{
						guild.icon
						&& (
							<AvatarImage
								src={`https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.${guild.icon.startsWith('a_') ? 'gif' : 'webp'}?size=128`}
							/>
						)
					}
					<AvatarFallback>
						{guild.name.replace(/(\W_)+/g, '').split(' ').map((str) => str[0]).join('')}
					</AvatarFallback>
				</Avatar>
				<span>{guild.name}</span>
			</h1>
			<hr className="mt-4 border-b border-black/20 dark:border-white/30" />

			<div className="mt-4">
				<div className="flex flex-col items-start gap-2">
					<Button variant="link">
						<Link href={`/guilds/${guild.id}`} className="flex gap-2 text-lg font-medium">
							<LucideHome />
							Home
						</Link>
					</Button>
					<Button variant="link">
						<Link href={`/guilds/${guild.id}/moderation`} className="flex gap-2 text-lg font-medium">
							<LucideHammer />
							Moderation
						</Link>
					</Button>
					<Button variant="link">
						<Link href={`/guilds/${guild.id}/logs`} className="flex gap-2 text-lg font-medium">
							<LucideScrollText />
							Logs
						</Link>
					</Button>
				</div>
			</div>
		</div>
	);
}
