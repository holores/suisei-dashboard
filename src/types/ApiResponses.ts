import type { ChannelType } from 'discord-api-types/v10';

export type RoleResponse = {
	id: string;
	name: string;
	color: number;
}[];

export type ChannelResponse = {
	id: string;
	name: string;
	type: ChannelType;
}[];

export interface StrikeConfigItem {
	action: 'WARN' | 'MUTE' | 'KICK' | 'BAN';
	duration: number | null;
}

export type ModerationConfigResponse = {
	guildId: string;
	strikes: StrikeConfigItem[];
	muteRole: string;
	logChannel: string;
	publicLogChannel: string | null;
	strikeExpiresAfter: number;
} | null;
