declare module '@retracedhq/logs-viewer' {
	import type { ReactNode } from 'react';

	export interface RetracedEventsBrowserProps {
		host?: string;
		auditLogToken: string;
		header?: string;
		customClass?: string;
		fields?: {
			label: string;
			type?: string;
			field?: string;
			getValue?: () => string;
			style?: CSSStyleDeclaration;
		}[];
		admin?: boolean;
		export?: boolean;
		crud?: string;
		theme?: 'light' | 'dark';
		apiTokenHelpURL?: string;
		searchHelpURL?: string;
		mount?: boolean;
	}

	export default function RetracedEventsBrowser(props: RetracedEventsBrowserProps): ReactNode;
}
