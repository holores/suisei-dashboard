import DiscordProvider from 'next-auth/providers/discord';
import { NextAuthOptions } from 'next-auth';
import { REST } from '@discordjs/rest';
import { RESTGetAPICurrentUserGuildsResult, Routes } from 'discord-api-types/v10';
import type { JWT } from 'next-auth/jwt';

declare module 'next-auth' {
	interface Session {
		user?: {
			id?: string | null;
			name?: string | null;
			email?: string | null;
			image?: string | null;
			access_token?: string;
		}
		error?: string;
	}
}

declare module 'next-auth/jwt' {
	// eslint-disable-next-line @typescript-eslint/no-shadow
	interface JWT {
		id: string;
		access_token: string;
		refresh_token: string;
		expires_at: number;
		error?: string;
	}
}

const acceptedGuilds = (process.env.ALLOWED_GUILDS as string).split(',').map((id) => id.trim());

const authOptions: NextAuthOptions = {
	providers: [
		DiscordProvider({
			clientId: process.env.DISCORD_CLIENT_ID as string,
			clientSecret: process.env.DISCORD_CLIENT_SECRET as string,
			authorization: 'https://discord.com/api/oauth2/authorize?scope=identify+guilds',
		}),
	],
	session: {
		strategy: 'jwt',
	},
	pages: {
		signIn: '/',
	},
	callbacks: {
		async signIn({ account }) {
			if (!account || account.provider !== 'discord') return false;

			const rest = new REST({
				authPrefix: 'Bearer',
				version: '10',
			}).setToken(account.access_token!);

			const guilds = await rest.get(Routes.userGuilds()) as RESTGetAPICurrentUserGuildsResult;

			const guildIds = guilds.map((guild) => guild.id);

			// The length of the array is short enough that short circuiting does not make a difference
			return acceptedGuilds.map((id) => guildIds.includes(id)).includes(true);
		},
		async jwt({ token, profile, account }): Promise<JWT> {
			if (account && profile) {
				// Save the access token and refresh token in the JWT on the initial login
				return {
					// eslint-disable-next-line max-len
					// @ts-expect-error id doesn't exist on normal OpenID profile, but Discord uses id instead of sub
					id: profile.id,
					access_token: account.access_token!,
					expires_at: account.expires_at!,
					refresh_token: account.refresh_token!,
				};
			} if (Date.now() < token.expires_at * 1000) {
				// If the access token has not expired yet, return it
				return token;
			}
			// If the access token has expired, try to refresh it
			try {
				const response = await fetch('https://discord.com/api/v10/oauth2/token', {
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
					body: new URLSearchParams({
						client_id: process.env.DISCORD_CLIENT_ID as string,
						client_secret: process.env.DISCORD_CLIENT_SECRET as string,
						grant_type: 'refresh_token',
						refresh_token: token.refresh_token,
					}),
					method: 'POST',
				});

				const tokens: any = await response.json();

				if (!response.ok) throw tokens;

				return {
					...token, // Keep the previous token properties
					access_token: tokens.access_token,
					expires_at: tokens.expires_at,
					// Fall back to old refresh token, but note that
					// many providers may only allow using a refresh token once.
					refresh_token: tokens.refresh_token ?? token.refresh_token,
				};
			} catch (error) {
				// eslint-disable-next-line no-console
				console.error('Error refreshing access token', error);
				// The error property will be used client-side to handle the refresh token error
				return { ...token, error: 'RefreshAccessTokenError' as const };
			}
		},
		async session({ session, token }) {
			/* eslint-disable no-param-reassign */
			if (session.user) {
				session.user.id = token.id;
				session.user.access_token = token.access_token;
			}
			session.error = token.error;
			/* eslint-enable */

			return session;
		},
	},
};

export default authOptions;
